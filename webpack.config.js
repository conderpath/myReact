const path = require("path")
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
module.exports = {
	entry: './src/app.jsx',
	output: {
		// publicPath:"/dist/",
		path: path.resolve(__dirname, "dist"),
		filename: 'js/App.js'
	},
	mode:"development",
	module: {
		rules: [
			{
	      test: /\.jsx$/,
	      exclude: /node_modules/,
	      use: 'babel-loader'
	      /*use: {
	        loader: 'babel-loader',
	        options: {
	          presets: ['@babel/preset-env',"react"]
	        }
	      }*/
	    },
			{
				test: /\.css$/,
				use: ['style-loader', 'css-loader']
			}, {
				test: /\.(png|jpg|gif)$/i,
				use: [{
					loader: 'url-loader',
					options: {
						limit: 8192
					}
				}]
			}, {
				test: /\.(eot|svg|ttf|woff|woff2|otf)$/i,
				use: [{
					loader: 'url-loader',
					options: {
						limit: 8192
					}
				}]
			}
		]
	},
	resolve: {
		alias:{
			src:path.resolve(__dirname,'src')
		},
		extensions: ['.js', '.jsx', '.json']
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: "index.html"
		}),
		new CopyPlugin([
	      { from: './src/mock', to: './mock' },
	    ])
	],
	devServer: {
		contentBase: path.join(__dirname, 'dist'),
		proxy: {
			'/api':{
				target:"https://c.y.qq.com/musichall",
				changeOrigin: true,
				pathRewrite: {'^/api' : ''}
			}
		},
		// compress: true,
		port: 9000/*,
		historyApiFallback: {
			index: '/dist/'
		}*/
	}
};