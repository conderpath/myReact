import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Link, NavLink, Redirect,Switch} from "react-router-dom"
import './../static/styles/reset.css'
import './../static/styles/index.css'
import NotFound from './pages/404.jsx'
import Index from './pages/index/index.jsx'
import Product from './pages/goods/product.jsx'
import User from './pages/user/user.jsx'
import UserDetail from './pages/user/userDetail.jsx'
import routerMap from './router.js'
console.log(routerMap,'sadfsdafasd')
class AppRouter extends Component{
	render() {
		return (
			<Router>
				<div className="wrapper">
					<div className="top-nav">
					</div>
					<div className="content">
						<div className="side-nav">
							<ul className="clearfix menu">
								<li className="item">
									<NavLink to="/">首页</NavLink>
								</li>
								<li className="item">
									<NavLink to="/product" activeClassName="active">
										<span>商品管理</span>
									</NavLink>
									<ul className="subMenu">
										<li>
											<NavLink to="/product">商品列表</NavLink>
										</li>
										<li>
											<NavLink to="/productTypes">商品分类</NavLink>
										</li>
									</ul>
								</li>
		            			<li className="item"><NavLink to="/user">用户</NavLink></li>
								<li className="item"><NavLink to="/user/123">用户详情</NavLink></li>
							</ul>
						</div>
						<div>
							<Switch>
					    		{/*<Route exact path="/" component={Index}></Route>
					    		<Route path="/product"   render={()=>{
					    			return <Redirect to="/"></Redirect>
					    		}}></Route>
					            <Route exact path="/user" component={User}></Route>
					    		<Route  path="/user/:id"  component={UserDetail}></Route>*/}
					    		{
					    			routerMap.map((item,index)=>{
					    				return <Route exact path={item.path} key={index} render={props=>{
					    					return (item.auth?<Redirect to="/" {...props}/>:(<item.component {...props}/>))
					    				}}/>
					    			})
					    		}
				             	<Route component={NotFound}/>
		        			</Switch>
		        		</div>
	        		</div>
      			</div>
			</Router>
		)
	}
}
ReactDOM.render(
  <AppRouter/>,
  document.getElementById('app')
);