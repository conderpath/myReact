import styled from 'styled-components';
export const Wraper = styled.div`
	position:fixed;
    top:0;
    left:0;
    width:100%;
    height:100%;
    background:rgba(0,0,0,.5);
    .detail{
        position:absolute;
        top:50%;
        left:50%;
        width:50%;
        height:50%;
        background:#fff;
        transform:translate(-50%,-50%);
    }
`;

export const Button = styled.button`
	// 根据props是否用primary来设置颜色和背景颜色
    background: ${props => props.primary ? 'palevioletred' : 'white'};
    color: ${props => props.primary ? 'white' : 'palevioletred'};
	font-size: 1em;
    margin: 1em;
    padding: 0.25em 1em;
    border: 2px solid palevioletred;
    border-radius: 3px;
`;