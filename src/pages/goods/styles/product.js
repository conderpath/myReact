import styled from 'styled-components';
export const Title = styled.h1`
	text-align:center;
	font-size:20px;
	color:red;
`;

export const Button = styled.button`
	// 根据props是否用primary来设置颜色和背景颜色
    background: ${props => props.primary ? 'palevioletred' : 'white'};
    color: ${props => props.primary ? 'white' : 'palevioletred'};
	font-size: 1em;
    margin: 1em;
    padding: 0.25em 1em;
    border: 2px solid palevioletred;
    border-radius: 3px;
`;