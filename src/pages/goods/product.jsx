import React, {Component} from 'react'
import {Route,Link} from 'react-router-dom'
import axios from 'axios'
import ProductDetail from './productDetail'
import store from '../../redux/index'
import { connect } from 'react-redux'
import {Title,Button} from './styles/product.js'
import {ASYSC_QUERY_PRODUCT_CREATOR,DELETE_PRODUCT_CREATOR} from "src/redux/actionCreators"
class Product extends Component {
	constructor(props) {
		super(props)
		this.state = {
			showDetail: false,
			list:[],
		}
		this.showDetail = this.showDetail.bind(this);
		this.hideDetail = this.hideDetail.bind(this);
		store.subscribe(() => {
			console.log(store.getState())
		})
	}
	handleIncrease() {
		store.dispatch({
			type:"INCREMENT"
		})
	}
	handleDecrease() {
		store.dispatch({
			type:"DECREMENT"
		})
	}
	showDetail() {
		this.setState({
			showDetail: true
		})
	}
	hideDetail(){
		this.setState({
			showDetail:false
		})
	}
	render() {
		let detail = null;
		if(this.state.showDetail) {
			detail = <ProductDetail id="132" hide={this.hideDetail}/>
		}
		return (
			<div>
				<table style={{width:"100%",textAlign:"center",marginBottom:"20px"}}>
					<thead>
						<tr>
							<th>商品id</th>
							<th>商品名称</th>
							<th>商品价格</th>
							<th>商品描述</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						{
							this.props.productList.length?
							(this.props.productList.map((item,index)=>(
								<tr key={item.id}>
									<td>{item.id}</td>
									<td>{item.name}</td>
									<td>{item.price}</td>
									<td>{item.memo}</td>
									<td><Button onClick={()=>this.props.delProduct(index)}>删除</Button></td>
								</tr>)
							)):<tr><td colSpan="5">暂无数据</td></tr>
						}
					</tbody>
				</table>
				{detail}
				<Title>信息</Title>
				<div>{JSON.stringify(this.props.match)}</div>
				<button onClick={this.showDetail}>商品详情</button>
				<Link to="/"><Button primary>回到首页</Button></Link>
				<Link to="/user"><Button>用户界面</Button></Link>
				<Button onClick={this.props.decrease}>减少</Button>
				<span>{this.props.count}</span>
				<Button onClick={this.props.increase}>添加</Button>
			</div>
		)
	}
	componentWillMount() {
		axios.get('/api/fcgi-bin/fcg_yqqhomepagerecommend.fcg',{
			params:{
				_: 1560499879199,
				g_tk: 5381,
				uin: 0,
				format: "json",
				inCharset: "utf-8",
				outCharset: "utf-8",
				notice: 0,
				platform: "h5",
				needNewCode: 1,
			}
		}).then(rst=>{
			console.log(rst,12312)
		})
		/*axios.get("/mock/product.json").then(rst=>{
			console.log(rst)
		})*/
		store.dispatch(ASYSC_QUERY_PRODUCT_CREATOR())
	}
}
//将redux store中的state映射到组件的props上
const mapStateToProps = state => {
	return {
		count:state.count,
		productList:state.product
	}
}
const mapDispatchToProps = dispatch=>{
	return {
		increase:()=> dispatch({type:"INCREMENT"}),
		decrease:()=> dispatch({type:'DECREMENT'}),
		delProduct:(idx)=>dispatch(DELETE_PRODUCT_CREATOR(idx))
	}
}
/*const mapDispatchToProps = ({
  onTodoClick: toggleTodo
})*/
Product = connect(mapStateToProps,mapDispatchToProps)(Product)

export default Product