import React, {Component} from 'react'
import {Wraper} from './styles/productDetail.js'
export default class ProductDetail extends Component {
	constructor(props) {
		super(props)
		this.hideDetail = this.hideDetail.bind(this);
		this.handleClick = this.handleClick.bind(this);
	}
	hideDetail() {
		this.props.hide()
	}
	handleClick(e) {
		e.cancelBubble = true;
		e.stopPropagation();
		console.log(1)
	}
	render() {
		return (
			<Wraper onClick={this.hideDetail}>
				<div className="detail" onClick={this.handleClick} ref="dd">
					商品ID：{this.props.id}
				</div>
			</Wraper>
		)
	}
	componentDidMount() {
	}
}
