import React, {Component} from 'react'
import {Link} from 'react-router-dom'
export class User extends Component {
	constructor(props) {
		super(props)
		this.refId = React.createRef()
	}
	render() {
		return (
			<div className="user" ref={this.refId}>
				<div>
					<div>个人中心</div>
					<div>
						{JSON.stringify(this.props.match)}
					</div>
					<div>
						<button></button>
					</div>
					<ul>
						<li>
							<Link to="/">回到首页</Link>
						</li>
						<li>
							<Link to="/userDetail/123">用户详情</Link>
						</li>
					</ul>
				</div>
			</div>
		)
	}
	componentDidMount() {
		console.log(this.refId.current)
	}
}
export default User