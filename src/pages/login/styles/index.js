import styled from 'styled-components';
export const Wraper = styled.div`
	position: absolute;
	top:0;
	bottom:0;
	left:0;
	right:0;
	width:40%;
	height:50%;
	margin: auto;
	border-radius:10px;
	background: #eee;
	text-align: center;
	.form-group{
		display:flex;
		height:100%;
		width:100%;
		flex-direction:column;
		justify-content:center;
		align-items:center;
	}	
	.form{
		height:40px;
		margin-top:20px;
		line-height:40px;
		label{
			display:inline-block;
			width:60px;
		}
		input{
			display:inline-block
			height:30px;
		}
	}
`;

export const Button = styled.button`
	// 根据props是否用primary来设置颜色和背景颜色
    background: ${props => props.primary ? 'palevioletred' : 'white'};
    color: ${props => props.primary ? 'white' : 'palevioletred'};
	font-size: 1em;
    margin: 1em;
    padding: 0.25em 1em;
    border: 2px solid palevioletred;
    border-radius: 3px;
`;