import React,{Component} from 'react'
import {Wraper,Button} from './styles/'
export default class Login extends Component{
	constructor(props) {
		super(props)
		this.handleLogin = this.handleLogin.bind(this)
	}
	handleLogin() {
		localStorage.login = true
		location.hash="/"
	}
	render() {
		return (
			<Wraper>
				<div className="form-group">
					<div className="form">
						<label htmlFor="">用户名</label><input type="text"/>
					</div>
					<div className="form">
						<label htmlFor="">密码</label><input type="password"/>
					</div>
					<div>
						<Button primary onClick={this.handleLogin}>登录</Button>
					</div>
				</div>
			</Wraper>
		)
	}
}