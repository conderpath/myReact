import React, {Component} from 'react'
import {Route,Link,Switch,Redirect} from 'react-router-dom'
import NotFound from '../404.jsx'
import Home from "../home/home"
import Product from '../goods/product.jsx'
import User from '../user/user.jsx'
import UserDetail from '../user/userDetail.jsx'
import {Wraper} from './styles/index'
export default class Index extends Component{
	constructor(props) {
		super(props)
	}
	render() {
		return (
			<Wraper>
				<header></header>
				<div className="content-box">
					<div className="nav-bar">
						<ul>
							<li><Link to="/home">首页</Link></li>
							<li><Link to="/user">用户中心</Link></li>
							<li><Link to="/product">产品中心</Link></li>
						</ul>
					</div>
					<div className="content">
						<Switch>
				    		<Route path="/home" component={Home}></Route>
				    		<Route path="/product" component={Product}></Route>
				    		<Route path="/user" component={User}></Route>
				    		<Route path="/userDetail/:id" component={UserDetail}></Route>
			             	<Redirect to="/home"></Redirect>
						</Switch>
					</div>
				</div>
				
			</Wraper>
		)
	}
}
