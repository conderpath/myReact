import styled from 'styled-components';
export const Wraper = styled.div`
	height:100%;
	width:100%;
	header{
		position:fixed;
		top:0;
		left:0;
		width:100%;
		height:50px;
		line-height:50px;
		background:#b3c0d1;
	}
	.content-box{
		display:flex;
		height:100%;
		padding-top:50px;
		box-sizing:border-box;
		.nav-bar{
			width:200px;
			background:#fff;
			border-right: 1px solid #eee;
			li{
				text-align:center;
				margin-top:10px;
				a{
					text-decoration:none;
					color:#000;
				}
			}
		}
		.content{
			flex:1
		}
	}
`;