import React, {Component} from 'react'
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import store from "./redux/index"
import { HashRouter as Router, Route, Link, NavLink, Redirect,Switch} from "react-router-dom"
import './../static/styles/reset.css'
import './../static/styles/index.css'

import Login from './pages/login/index'
import Index from './pages/index/index'
import NotFound from './pages/404.jsx'
import routerMap from './router.js'
import Product from './pages/goods/product.jsx'
import User from './pages/user/user.jsx'
import UserDetail from './pages/user/userDetail.jsx'
class App extends Component{
	render() {
		return (
			<Router>
				<Switch>
					<Route path="/login" component={Login}></Route>
					<Route path="/" render={(props)=> {
						return (localStorage.login?<Index {...props}/>:<Redirect to="/login"/>)
					}}></Route>
				</Switch>
			</Router>
		)
	}
}

ReactDOM.render(
	<Provider store={store}><App/></Provider>,
	document.getElementById("app")
)