import Login from 'src/pages/login/index'
import Index from 'src/pages/index/index'
import User from 'src/pages/user/user'
import UserDetail from 'src/pages/user/userDetail'
import Product from 'src/pages/goods/product'
export default [
	{
		path:"/login",
		component: Login
	},
	{
		path:"/",
		component:Index
	},
	{
		path:"/user",
		component:User,
		auth:true
	},
	{
		path:"/user/:id",
		component:UserDetail
	},
	{
		path:"/product",
		component:Product
	}
]