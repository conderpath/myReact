import {combineReducers, createStore,applyMiddleware } from 'redux'
import {createLogger} from 'redux-logger';
import thunk from 'redux-thunk';
import todo from "./todo"
import count from "./count"
import product from './product'
const logger = createLogger();
/*function counter(state = 0, action) {
  switch (action.type) {
    case 'INCREMENT':
      return state + 1
    case 'DECREMENT':
      return state - 1
    default:
      return state
  }
}*/
let reducers = combineReducers({todo,count,product})
let store = createStore(reducers,applyMiddleware(thunk,logger))

// let store = createStore(reducers)
/*let next = store.dispatch;
store.dispatch = function(action) {
	console.log("开始了")
	next(action);
	console.log(store.getState())
	console.log('结束了')
}*/
export default store;