import {
	QUERY_PRODUCT,
	ADD_PRODUCT,
	DELETE_PRODUCT
} from "./actionTypes"
export default function todo(state=[],action) {
	switch(action.type) {
		case "QUERY_PRODUCT":
			return action.data;
		case "ADD_PRODUCT":
			return state.concat(action.data)
		case "DELETE_PRODUCT":
			return state.filter((item,idx)=>{
				return idx!==action.index
			})
		default:
			return state;
	}
}