import {
	QUERY_PRODUCT,
	ADD_PRODUCT,
	DELETE_PRODUCT
} from "./actionTypes"
import axios from 'axios'
export const QUERY_PRODUCT_CREATOR = (data) => (
	{
		type:QUERY_PRODUCT,
		data
	}
)
export const  ASYSC_QUERY_PRODUCT_CREATOR = () =>{
	return (dispatch) => {
		axios.get("/mock/product.json").then(rst=>{
			let action = QUERY_PRODUCT_CREATOR(rst.data);
			dispatch(action)
		})
	}
}
export const ADD_PRODUCT_CREATOR = (data) => (
	{
		type:ADD_PRODUCT,
		data
	}
)

export const DELETE_PRODUCT_CREATOR = (index) => (
	{
		type:DELETE_PRODUCT,
		index
	}
)