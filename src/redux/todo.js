var list = [
	{
		name:'振宇',
		sex:'M'
	},
	{
		name:'一诺',
		sex:'F'
	}
]
export default function todo(state=list,action) {
	switch(action.type) {
		case "M":
			return state.filter(item=>item.sex==="M");
		case "F":
			return state.filter(item=>item.sex==="F");
		default:
			return state;
	}
}